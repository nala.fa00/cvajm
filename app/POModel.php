<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class POModel extends Model
{
    protected $table = 'po';
    protected $fillable = [
        'id', 'id_po', 'nama_po', 'id_proyek', 'nama_perusahaan', 'mulai', 'selesai', 'biaya', 'status',
    ];
    public $timestamps = false;
}
