<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranModel extends Model
{
    protected $table = 'pengeluaran';
    protected $fillable = [
        'id', 'id_pengeluaran', 'id_aktivitas', 'nilai', 'deskripsi', 'status',
    ];
    public $timestamps = false;
}
