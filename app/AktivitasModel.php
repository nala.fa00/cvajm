<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AktivitasModel extends Model
{
    protected $table = 'aktivitas';
    protected $fillable = [
        'id', 'id_aktivitas', 'nama', 'id_po', 'mulai', 'selesai', 'kegiatan', 'status',
    ];
    public $timestamps = false;
}
