<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyekModel extends Model
{
    protected $table = 'proyek';
    protected $fillable = [
        'id', 'id_proyek', 'id_koordinator', 'nama', 'mulai', 'selesai', 'status',
    ];
    public $timestamps = false;
}
