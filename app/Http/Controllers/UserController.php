<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;

class UserController extends Controller
{
    public function postAddUser(Request $request)
    {
    	$this->validate($request, [
			'nama' => 'required',
			'jabatan' => 'required',
		]);

		$users = new User();
		$users->nama = $request['nama'];
		$users->password = bcrypt('cvajm');
		$users->jabatan = $request['jabatan'];

		$users->save();

		return redirect()->back()->with(['messagesukses' => 'User Sukses Dibuat',
											'messagesukses2' => 'Segera Ubah Password User Baru'
		]);
    }

    public function getDeleteUser($id)
    {
    	$user= User::find($id);
    	$user->delete();
    	return redirect()->back()->with(['messagesukses' => 'User Sukses Dihapus']);
    }

    public function getEditUser($id)
    {
    	$user = User::find($id);
    	return view('user.ubahuser')->with(['datauser' => $user] );
    }

    public function postEditUser(Request $request, $id)
    {
    	$user = User::find($id);
    	$this->validate($request, [
    		'jabatan' => 'required',
    	]);

    	$user->jabatan = $request['jabatan'];
    	$user->save();
    	return redirect()->route('manajemenuser')->with(['messagesukses' => 'User Sukses Diubah']);

    }
}
