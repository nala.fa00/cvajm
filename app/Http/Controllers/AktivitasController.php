<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AktivitasModel;
use App\DetailAktivitasModel;
use App\KaryawanModel;
use App\POModel;
use DB;

class AktivitasController extends Controller
{
    public function postAddAktivitas(Request $request)
    {
    	$this->validate($request, [
    		'idaktivitas' => 'required',
			'namaaktivitas' => 'required',
			'po' => 'required',
			'tanggalmulai' => 'required',
			'tanggalselesai' => 'required',
			'kegiatan' => 'required',
			'status' => 'required',
		]);
    	
		$Aktivitas = new AktivitasModel();
		$Aktivitas->id_aktivitas = $request['idaktivitas'];
		$Aktivitas->nama = $request['namaaktivitas'];
		$Aktivitas->id_po = $request['po'];
		$Aktivitas->mulai = $request['tanggalmulai'];
		$Aktivitas->selesai = $request['tanggalselesai'];
		$Aktivitas->kegiatan = $request['kegiatan'];
		$Aktivitas->status = $request['status'];
		
		$Aktivitas->save();

		return redirect()->back()->with(['messagesukses' => 'Aktivitas Sukses Disimpan']);
    }

    public function getDeleteAktivitas($id)
    {
    	$Aktivitas= AktivitasModel::find($id);
    	$Aktivitas->delete();
    	return redirect()->back()->with(['messagesukses' => 'Aktivitas Sukses Dihapus']);
    }

    public function getEditAktivitas($id)
    {
    	$aktivitas = DB::table('aktivitas')
                        ->join('po', 'aktivitas.id_po', '=', 'po.id')
                        ->select('aktivitas.*', 'po.nama_po as namapo')
                        ->where('aktivitas.id', $id)
                        ->first();

        $datapo = POModel::where('status', '=', 'Berjalan')->get();

    	return view('po.ubahaktivitas')->with(['dataaktivitas' => $aktivitas, 'datapo'=>$datapo] );
    }

    public function postEditAktivitas(Request $request, $id)
    {
    	$Aktivitas = AktivitasModel::find($id);
    	
    	$this->validate($request, [
    		'idaktivitas' => 'required',
			'namaaktivitas' => 'required',
			'po' => 'required',
			'tanggalmulai' => 'required',
			'tanggalselesai' => 'required',
    	]);

    	$Aktivitas->id_aktivitas = $request['idaktivitas'];
		$Aktivitas->nama = $request['namaaktivitas'];
		$Aktivitas->id_po = $request['po'];
		$Aktivitas->mulai = $request['tanggalmulai'];
		$Aktivitas->selesai = $request['tanggalselesai'];
		$Aktivitas->kegiatan = $request['kegiatan'];
		$Aktivitas->status = $request['status'];
		
    	$Aktivitas->save();

    	return redirect()->route('manajemenaktivitas')->with(['messagesukses' => 'Aktivitas Sukses Diubah']);
		
    }

    public function getAktivitas($id)
    {
        $aktivitas = AktivitasModel::find($id);

        $karyawan = DB::table('karyawan')
        				->where('jabatan', '<>', 'Eksekutif')
        				->where('jabatan', '<>', 'Administrasi')
        				->where('jabatan', '<>', 'Pimpinan Proyek')
        				->where('status', '=', 'Idle')
        				->get();

        $assign = DB::table('detailaktivitas')
                        ->join('aktivitas', 'detailaktivitas.id_aktivitas', '=', 'aktivitas.id')
                        ->join('karyawan', 'detailaktivitas.id_karyawan', '=', 'karyawan.id')
                        ->select('detailaktivitas.*', 'aktivitas.nama', 'karyawan.nama', 'karyawan.NIK', 'karyawan.id as idkar')
                        ->where('aktivitas.id', $id)
                        ->get();

    	return view('po.assigntk')->with(['aktivitas' => $aktivitas, 'karyawan' => $karyawan, 'assign' => $assign] );
    }

    public function postAddTenagaKerja(Request $request, $id, $id2)
    {

    	$DetailAktivitas = new DetailAktivitasModel();
		$DetailAktivitas->id_aktivitas = $id2;
		$DetailAktivitas->id_karyawan = $id;
		
		$DetailAktivitas->save();

		$karyawan = KaryawanModel::find($id);
		$karyawan->status = 'Bekerja';
		$karyawan->save();

		return redirect()->back()->with(['messagesukses' => 'Tenaga Kerja Sukses Di-assign']);
    }

    public function getDeleteTenagaKerja($id, $id2)
    {
    	$DetailAktivitas= DetailAktivitasModel::find($id);
    	$DetailAktivitas->delete();

    	$karyawan = KaryawanModel::find($id2);
		$karyawan->status = 'Idle';
		$karyawan->save();

    	return redirect()->back()->with(['messagesukses' => 'Tenaga Kerja Sukses Dikeluarkan']);
    }
}
