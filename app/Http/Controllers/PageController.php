<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\KaryawanModel;
use App\ProyekModel;
use App\POModel;
use App\AktivitasModel;
use App\PengeluaranModel;
use DB;

class PageController extends Controller
{
	public function getWelcome(){
    	return view('welcome');
    }

    public function getHome(){
    	return view('halamanutama');
    }

    public function getManajemenUser(){
        $users=User::all();
    	return view('user.manajemenuser')->with(['user'=>$users]);
    }

	public function getManajemenKaryawan(){
        $karyawan = KaryawanModel::all();

        $idKaryawan = KaryawanModel::orderBy('id', 'desc')->first();
        $pisah = explode('-', $idKaryawan->NIK);

        if ($pisah[0] <> date('Y')) {
            $noselanjutnya = date('Y').'-'.'0001';
        } else {
            $nobaru = (int)$pisah[1]+1;

            if ($nobaru < 9) {
                $noskrg = '000'.$nobaru;
            } elseif ($nobaru < 99) {
                $noskrg = '00'.$nobaru;
            } elseif ($nobaru < 999) {
                $noskrg = '0'.$nobaru;
            } elseif ($nobaru < 9999) {
                $noskrg = $nobaru;
            }
            $noselanjutnya = date('Y').'-'.$noskrg;
        }

    	return view('karyawan.manajemenkaryawan')->with(['karyawan'=>$karyawan, 'noselanjutnya'=>$noselanjutnya]);
    }    

	public function getManajemenProyek(){
        //$proyek = ProyekModel::all();

        $proyek = DB::table('proyek')
                        ->join('karyawan', 'proyek.id_koordinator', '=', 'karyawan.id')
                        ->select('proyek.*', 'karyawan.nama as namakoor')
                        ->get();

        $idProyek = ProyekModel::orderBy('id', 'desc')->first();
        $pisah = explode('-', $idProyek->id_proyek);

        if ($pisah[1] <> date('Y')) {
            $noselanjutnya ='PR-'.date('Y').'-'.'0001';
        } else {
            $nobaru = (int)$pisah[2]+1;

            if ($nobaru < 9) {
                $noskrg = '000'.$nobaru;
            } elseif ($nobaru < 99) {
                $noskrg = '00'.$nobaru;
            } elseif ($nobaru < 999) {
                $noskrg = '0'.$nobaru;
            } elseif ($nobaru < 9999) {
                $noskrg = $nobaru;
            }
            $noselanjutnya = 'PR-'.date('Y').'-'.$noskrg;
        }
        
        $datakoordinator = KaryawanModel::where('jabatan', '=', 'Pimpinan Proyek')->get();

    	return view('proyek.manajemenproyek')->with(['proyek'=>$proyek, 'datakoordinator'=>$datakoordinator, 'noselanjutnya'=>$noselanjutnya]);
    }

    public function getPengeluaranProyek(){
        $proyek = DB::table('proyek')
                    ->where('status', '=', 'Berjalan')
                    ->get();

    	return view('proyek.pengeluaranproyek')->with(['proyek'=>$proyek]);
    }

	public function getManajemenPO(){
        $po = DB::table('po')
                        ->join('proyek', 'po.id_proyek', '=', 'proyek.id')
                        ->select('po.*', 'proyek.nama as namapro')
                        ->get();

        $idPO = POModel::orderBy('id', 'desc')->first();
        $pisah = explode('-', $idPO->id_po);

        if ($pisah[1] <> date('Y')) {
            $noselanjutnya ='PO-'.date('Y').'-'.'0001';
        } else {
            $nobaru = (int)$pisah[2]+1;

            if ($nobaru < 9) {
                $noskrg = '000'.$nobaru;
            } elseif ($nobaru < 99) {
                $noskrg = '00'.$nobaru;
            } elseif ($nobaru < 999) {
                $noskrg = '0'.$nobaru;
            } elseif ($nobaru < 9999) {
                $noskrg = $nobaru;
            }
            $noselanjutnya = 'PO-'.date('Y').'-'.$noskrg;
        }
        
        $dataproyek = ProyekModel::where('status', '=', 'Berjalan')->get();

    	return view('po.manajemenpo')->with(['po'=>$po, 'dataproyek'=>$dataproyek, 'noselanjutnya'=>$noselanjutnya]);
    }

    public function getManajemenAktivitas(){
        $aktivitas = DB::table('aktivitas')
                        ->join('po', 'aktivitas.id_po', '=', 'po.id')
                        ->join('proyek', 'po.id_proyek', '=', 'proyek.id')
                        ->select('aktivitas.*', 'po.nama_po as namapo', 'proyek.nama as namapro')
                        ->get();

        $idAktivitas = AktivitasModel::orderBy('id', 'desc')->first();
        $pisah = explode('-', $idAktivitas->id_aktivitas);

        if ($pisah[1] <> date('Y')) {
            $noselanjutnya ='AK-'.date('Y').'-'.'0001';
        } else {
            $nobaru = (int)$pisah[2]+1;

            if ($nobaru < 9) {
                $noskrg = '000'.$nobaru;
            } elseif ($nobaru < 99) {
                $noskrg = '00'.$nobaru;
            } elseif ($nobaru < 999) {
                $noskrg = '0'.$nobaru;
            } elseif ($nobaru < 9999) {
                $noskrg = $nobaru;
            }
            $noselanjutnya = 'AK-'.date('Y').'-'.$noskrg;
        }
        
        $datapo = POModel::where('status', '=', 'Berjalan')->get();

    	return view('po.aktivitas')->with(['aktivitas'=>$aktivitas, 'datapo'=>$datapo, 'noselanjutnya'=>$noselanjutnya]);
    }

    public function getApprovalPengeluaran(){
        $pengeluaran = DB::table('pengeluaran')
                        ->join('aktivitas', 'pengeluaran.id_aktivitas', '=', 'aktivitas.id')
                        ->join('po', 'aktivitas.id_po', '=', 'po.id')
                        ->join('proyek', 'po.id_proyek', '=', 'proyek.id')
                        ->select('pengeluaran.*', 'aktivitas.nama as namaak', 'po.nama_po', 'proyek.nama as namapro')
                        ->where('pengeluaran.status', '=', 'Pending')
                        ->get();

        return view('approval.approval')->with(['pengeluaran'=>$pengeluaran]);
    }

    public function getListPengeluaran(){
        $pengeluaran = DB::table('pengeluaran')
                        ->join('aktivitas', 'pengeluaran.id_aktivitas', '=', 'aktivitas.id')
                        ->join('po', 'aktivitas.id_po', '=', 'po.id')
                        ->join('proyek', 'po.id_proyek', '=', 'proyek.id')
                        ->select('pengeluaran.*', 'aktivitas.nama as namaak', 'po.nama_po', 'proyek.nama as namapro', 'proyek.status as statuspro')
                        ->get();

        return view('proyek.listpengeluaran')->with(['pengeluaran'=>$pengeluaran]);
    }
}
