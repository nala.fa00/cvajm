<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProyekModel;
use App\POModel;
use DB;

class POController extends Controller
{
    public function postAddPO(Request $request)
    {
    	$this->validate($request, [
    		'idpo' => 'required',
			'namapo' => 'required',
			'proyek' => 'required',
			'tanggalmulai' => 'required',
			'tanggalselesai' => 'required',
		]);

		$PO = new POModel();
		$PO->id_po = $request['idpo'];
		$PO->nama_po = $request['namapo'];
		$PO->id_proyek = $request['proyek'];
		$PO->nama_perusahaan = $request['namaperusahaan'];
		$PO->mulai = $request['tanggalmulai'];
		$PO->selesai = $request['tanggalselesai'];
		$PO->biaya = $request['biaya'];
		$PO->status = $request['status'];
		
		$PO->save();

		return redirect()->back()->with(['messagesukses' => 'PO Sukses Disimpan']);
    }

    public function getDeletePO($id)
    {
    	$PO= POModel::find($id);
    	$PO->delete();
    	return redirect()->back()->with(['messagesukses' => 'PO Sukses Dihapus']);
    }

    public function getEditPO($id)
    {
    	$po = DB::table('po')
                        ->join('proyek', 'po.id_proyek', '=', 'proyek.id')
                        ->select('po.*', 'proyek.nama as namapro')
                        ->where('po.id', $id)
                        ->first();

        $dataproyek = ProyekModel::where('status', '=', 'Berjalan')->get();

    	return view('po.ubahpo')->with(['datapo' => $po, 'dataproyek'=>$dataproyek] );
    }

    public function postEditPO(Request $request, $id)
    {
    	$PO = POModel::find($id);
    	
    	$this->validate($request, [
    		'namapo' => 'required',
			'proyek' => 'required',
			'namaperusahaan' => 'required',
			'tanggalmulai' => 'required',
			'tanggalselesai' => 'required',
    	]);

    	$PO->nama_po = $request['namapo'];
		$PO->id_proyek = $request['proyek'];
		$PO->nama_perusahaan = $request['namaperusahaan'];
		$PO->mulai = $request['tanggalmulai'];
		$PO->selesai = $request['tanggalselesai'];
		$PO->biaya = $request['biaya'];
		$PO->status = $request['status'];

    	$PO->save();

    	return redirect()->route('manajemenpo')->with(['messagesukses' => 'PO Sukses Diubah']);
		
    }
}
