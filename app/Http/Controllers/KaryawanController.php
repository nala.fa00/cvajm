<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\KaryawanModel;

class KaryawanController extends Controller
{
    public function postAddKaryawan(Request $request)
    {
    	$this->validate($request, [
    		'nik' => 'required',
			'noktp' => 'required',
			'nama' => 'required',
			'jabatan' => 'required',
			'gaji' => 'required',
		]);

		$Karyawan = new KaryawanModel();
		$Karyawan->NIK = $request['nik'];
		$Karyawan->no_ktp = $request['noktp'];
		$Karyawan->nama = $request['nama'];
		$Karyawan->jabatan = $request['jabatan'];
		$Karyawan->gaji = $request['gaji'];
		$Karyawan->tunjangan = $request['tunjangan'];
		$Karyawan->status = 'Idle';

		$Karyawan->save();

		return redirect()->back()->with(['messagesukses' => 'Karyawan Sukses Disimpan']);
    }

    public function getDeleteKaryawan($id)
    {
    	$Karyawan= KaryawanModel::find($id);
    	$Karyawan->delete();
    	return redirect()->back()->with(['messagesukses' => 'Karyawan Sukses Dihapus']);
    }

    public function getEditKaryawan($id)
    {
    	$Karyawan = KaryawanModel::find($id);
    	return view('karyawan.ubahkaryawan')->with(['datakaryawan' => $Karyawan] );
    }

    public function postEditKaryawan(Request $request, $id)
    {
    	$Karyawan = KaryawanModel::find($id);
    	//	dd($Karyawan);

    	
    	$this->validate($request, [
    		'nama' => 'required',
    		'jabatan' => 'required',
    		'gaji' => 'required',
    	]);

    	$Karyawan->nama = $request['nama'];
		$Karyawan->jabatan = $request['jabatan'];
		$Karyawan->gaji = $request['gaji'];
		$Karyawan->tunjangan = $request['tunjangan'];
    	$Karyawan->save();

    	return redirect()->route('manajemenkaryawan')->with(['messagesukses' => 'Karyawan Sukses Diubah']);
		
    }
}
