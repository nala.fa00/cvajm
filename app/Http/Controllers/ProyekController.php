<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProyekModel;
use App\KaryawanModel;
use App\POModel;
use DB;

class ProyekController extends Controller
{
    public function postAddProyek(Request $request)
    {
    	$this->validate($request, [
    		'idproyek' => 'required',
			'namaproyek' => 'required',
			'koordinatorproyek' => 'required',
			'tanggalmulai' => 'required',
			'tanggalselesai' => 'required',
		]);

		$Proyek = new ProyekModel();
		$Proyek->id_proyek = $request['idproyek'];
		$Proyek->nama = $request['namaproyek'];
		$Proyek->mulai = $request['tanggalmulai'];
		$Proyek->selesai = $request['tanggalselesai'];
		$Proyek->status = $request['status'];
		$Proyek->id_koordinator = $request['koordinatorproyek'];

		$Proyek->save();

		return redirect()->back()->with(['messagesukses' => 'Proyek Sukses Disimpan']);
    }

    public function getDeleteProyek($id)
    {
    	$Proyek= ProyekModel::find($id);
        $Proyek->status = 'Cancel';
        $Proyek->save();

        $po = DB::table('po')
                ->where('id_proyek', '=', $id)
                ->update(['status' => 'Cancel']);

        $idpo = DB::table('po')
                ->where('id_proyek', '=', $id)
                ->get();

        foreach ($idpo as $idpo) {
            $aktivitas = DB::table('aktivitas')
                            ->where('id_po', '=', $idpo->id)
                            ->update(['status' => 'Cancel']);

            $idaktivitas = DB::table('aktivitas')
                            ->where('id_po', '=', $idpo->id)
                            ->get();
            foreach ($idaktivitas as $idaktivitas) {
                $idkaryawan = DB::table('detailaktivitas')
                                ->where('id_aktivitas', '=', $idaktivitas->id)
                                ->get();
                
                foreach ($idkaryawan as $idkaryawan) {
                    $karyawan = DB::table('karyawan')
                                    ->where('id', '=', $idkaryawan->id_karyawan)
                                    ->update(['status' => 'Idle']);
                }
            }
        }

    	//$Proyek->delete();
    	return redirect()->back()->with(['messagesukses' => 'Proyek Sukses Dicancel']);
    }

    public function getEditProyek($id)
    {
    	$proyek = DB::table('proyek')
                        ->join('karyawan', 'proyek.id_koordinator', '=', 'karyawan.id')
                        ->select('proyek.*', 'karyawan.nama as namakoor')
                        ->where('proyek.id', $id)
                        ->first();

        $datakoordinator = KaryawanModel::where('jabatan', '=', 'Pimpinan Proyek')->get();

    	return view('proyek.ubahproyek')->with(['dataproyek' => $proyek, 'datakoordinator'=>$datakoordinator] );
    }

    public function postEditProyek(Request $request, $id)
    {
    	$Proyek = ProyekModel::find($id);
    	//	dd($Proyek);

    	
    	$this->validate($request, [
    		'namaproyek' => 'required',
    		'tanggalmulai' => 'required',
    		'tanggalselesai' => 'required',
    	]);

    	$Proyek->nama = $request['nama'];
		$Proyek->nama = $request['namaproyek'];
		$Proyek->mulai = $request['tanggalmulai'];
		$Proyek->selesai = $request['tanggalselesai'];
		$Proyek->status = $request['status'];
		$Proyek->id_koordinator = $request['koordinatorproyek'];
    	$Proyek->save();

    	return redirect()->route('manajemenproyek')->with(['messagesukses' => 'Proyek Sukses Diubah']);
		
    }

    public function getDetailProyek($id)
    {
        $proyek = DB::table('proyek')
                        ->join('karyawan', 'proyek.id_koordinator', '=', 'karyawan.id')
                        ->select('proyek.*', 'karyawan.nama as namakoor')
                        ->where('proyek.id', $id)
                        ->first();

        $po = DB::table('po')
                ->where('id_proyek', '=', $id)
                ->get();

        $aktivitas = DB::table('proyek')
                        ->join('po', 'proyek.id', '=', 'po.id_proyek')
                        ->join('aktivitas', 'po.id', '=', 'aktivitas.id_po')
                        ->select('proyek.*', 'aktivitas.id_aktivitas', 'aktivitas.nama as aknama', 'aktivitas.mulai as akmulai', 'aktivitas.selesai as akselesai', 'aktivitas.kegiatan as akkegiatan', 'aktivitas.status as akstatus')
                        ->where('proyek.id', $id)
                        ->get();

        $pengeluaran = DB::table('proyek')
                        ->join('po', 'proyek.id', '=', 'po.id_proyek')
                        ->join('aktivitas', 'po.id', '=', 'aktivitas.id_po')
                        ->join('pengeluaran', 'aktivitas.id', '=', 'pengeluaran.id_aktivitas')
                        ->select('proyek.*', 'aktivitas.id_aktivitas', 'pengeluaran.id_pengeluaran', 'pengeluaran.nilai', 'pengeluaran.deskripsi', 'pengeluaran.status as statuspeng')
                        ->where('proyek.id', $id)
                        ->get();
        
        return view('proyek.detailproyek')->with(['dataproyek' => $proyek, 'datapo' => $po, 'dataaktivitas' => $aktivitas, 'datapengeluaran' => $pengeluaran] );
    }
}
