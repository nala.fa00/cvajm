<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProyekModel;
use App\AktivitasModel;
use App\POModel;
use App\PengeluaranModel;
use DB;

class PengeluaranController extends Controller
{
    public function getPilihProyek($id)
    {
    	$proyek = ProyekModel::find($id);
    	$po = DB::table('po')
    			->where('id_proyek', '=', $id)
    			->get();
    	
    	return view('proyek.pengeluaranproyek2')->with(['proyek' => $proyek, 'po' => $po] );
    }

	public function getPilihPO($id, $id2)
    {
    	$proyek = ProyekModel::find($id);
 		$po = POModel::find($id2);
 		$aktivitas = DB::table('aktivitas')
    			->where('id_po', '=', $id2)
    			->get();
    	
    	return view('proyek.pengeluaranproyek3')->with(['proyek' => $proyek, 'po' => $po, 'aktivitas' => $aktivitas] );
    }

    public function getPilihAktivitas($id, $id2, $id3)
    {
    	$proyek = ProyekModel::find($id);
 		$po = POModel::find($id2);
 		$aktivitas = AktivitasModel::find($id3);
    	
    	$idPengeluaran = PengeluaranModel::orderBy('id', 'desc')->first();
        $pisah = explode('-', $idPengeluaran->id_pengeluaran);

        if ($pisah[1] <> date('Y')) {
            $noselanjutnya ='PO-'.date('Y').'-'.'0001';
        } else {
            $nobaru = (int)$pisah[2]+1;

            if ($nobaru < 9) {
                $noskrg = '000'.$nobaru;
            } elseif ($nobaru < 99) {
                $noskrg = '00'.$nobaru;
            } elseif ($nobaru < 999) {
                $noskrg = '0'.$nobaru;
            } elseif ($nobaru < 9999) {
                $noskrg = $nobaru;
            }
            $noselanjutnya = 'PG-'.date('Y').'-'.$noskrg;
        }

    	return view('proyek.pengeluaranproyek4')->with(['proyek' => $proyek, 'po' => $po, 'aktivitas' => $aktivitas, 'noselanjutnya' => $noselanjutnya] );
    }

    public function getSetujuiPengeluaran($id)
    {
    	
		$Pengeluaran = DB::table('pengeluaran')
							->where('id', $id)
							->update(['status' => 'Approved']);

		return redirect()->route('approvalpengeluaran')->with(['messagesukses' => 'Pengeluaran Sukses Disetujui']);
    }

    public function getBatalPengeluaran($id)
    {
    	
		$Pengeluaran = DB::table('pengeluaran')
							->where('id', $id)
							->update(['status' => 'Cancelled']);

		return redirect()->route('approvalpengeluaran')->with(['messagesukses' => 'Pengeluaran Tidak Disetujui']);
    }

    public function getEditPengeluaran($id)
    {
    	
		$pengeluaran = PengeluaranModel::find($id);

		return view('proyek.editpengeluaran')->with(['pengeluaran' => $pengeluaran]);
    }

    public function postEditPengeluaran(Request $request, $id)
    {
    	
		$pengeluaran = PengeluaranModel::find($id);
    	
    	$this->validate($request, [
    		'keperluan' => 'required',
			'nominal' => 'required',
    	]);

		$pengeluaran->deskripsi = $request['keperluan'];
		$pengeluaran->nilai = $request['nominal'];
		$pengeluaran->status = 'Pending';
		
    	$pengeluaran->save();

    	return redirect()->route('listpengeluaran')->with(['messagesukses' => 'Pengeluaran Sukses Diubah']);
    }

    public function postAddPengeluaran(Request $request)
    {
        
        $pengeluaran = new PengeluaranModel();
        
        $this->validate($request, [
            'keperluan' => 'required',
            'nominal' => 'required',
        ]);

        $pengeluaran->id_pengeluaran = $request['idpengeluaran'];
        $pengeluaran->id_aktivitas = $request['idak'];
        $pengeluaran->deskripsi = $request['keperluan'];
        $pengeluaran->nilai = $request['nominal'];
        $pengeluaran->status = 'Pending';
        
        $pengeluaran->save();

        return redirect()->route('pengeluaranproyek')->with(['messagesukses' => 'Pengeluaran Sukses Disimpan']);
    }
}
