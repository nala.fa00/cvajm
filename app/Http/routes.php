<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('halamanutama');
// });

Route::get('/', [
	'uses' => 'PageController@getWelcome',
	'as' => 'welcomex§'
]);

Route::post('/halamanutama', [
	'uses' => 'PageController@getHome',
	'as' => 'halamanutama'
]);

Route::get('/manajemenuser', [
	'uses' => 'PageController@getManajemenUser',
	'as' => 'manajemenuser'
]);

Route::get('/manajemenkaryawan', [
	'uses' => 'PageController@getManajemenKaryawan',
	'as' => 'manajemenkaryawan'
]);

Route::get('/manajemenproyek', [
	'uses' => 'PageController@getManajemenProyek',
	'as' => 'manajemenproyek'
]);

Route::get('/pengeluaranproyek', [
	'uses' => 'PageController@getPengeluaranProyek',
	'as' => 'pengeluaranproyek'
]);

Route::get('/manajemenpo', [
	'uses' => 'PageController@getManajemenPO',
	'as' => 'manajemenpo'
]);

Route::get('/manajemenaktivitas', [
	'uses' => 'PageController@getManajemenAktivitas',
	'as' => 'manajemenaktivitas'
]);

Route::get('/approvalpengeluaran', [
	'uses' => 'PageController@getApprovalPengeluaran',
	'as' => 'approvalpengeluaran'
]);

Route::get('/listpengeluaran', [
	'uses' => 'PageController@getListPengeluaran',
	'as' => 'listpengeluaran'
]);

//Start route manajemen user
Route::post('/simpanuser', [
	'uses' => 'UserController@postAddUser',
	'as' => 'simpanuser'
]);

Route::get('/hapususer/{id}', [
	'uses' => 'UserController@getDeleteUser',
	'as' => 'hapususer'
]);

Route::get('/ambiluser/{id}', [
	'uses' => 'UserController@getEditUser',
	'as' => 'ambiluser'
]);

Route::post('/updateuser/{id}', [
	'uses' => 'UserController@postEditUser',
	'as' => 'updateuser'
]);
//End route manajemen user

//Start route manajemen karyawan
Route::post('/simpankaryawan', [
	'uses' => 'KaryawanController@postAddKaryawan',
	'as' => 'simpankaryawan'
]);

Route::get('/hapuskaryawan/{id}', [
	'uses' => 'KaryawanController@getDeleteKaryawan',
	'as' => 'hapuskaryawan'
]);

Route::get('/ambilkaryawan/{id}', [
	'uses' => 'KaryawanController@getEditKaryawan',
	'as' => 'ambilkaryawan'
]);

Route::post('/updatekaryawan/{id}', [
	'uses' => 'KaryawanController@postEditKaryawan',
	'as' => 'updatekaryawan'
]);
//End route manajemen karyawan

//Start route manajemen proyek
Route::post('/simpanproyek', [
	'uses' => 'ProyekController@postAddProyek',
	'as' => 'simpanproyek'
]);

Route::get('/hapusproyek/{id}', [
	'uses' => 'ProyekController@getDeleteProyek',
	'as' => 'hapusproyek'
]);

Route::get('/ambilproyek/{id}', [
	'uses' => 'ProyekController@getEditProyek',
	'as' => 'ambilproyek'
]);

Route::post('/updateproyek/{id}', [
	'uses' => 'ProyekController@postEditProyek',
	'as' => 'updateproyek'
]);

Route::get('/detailproyek/{id}', [
	'uses' => 'ProyekController@getDetailProyek',
	'as' => 'detailproyek'
]);
//End route manajemen proyek

//Start route manajemen po
Route::post('/simpanpo', [
	'uses' => 'POController@postAddPO',
	'as' => 'simpanpo'
]);

Route::get('/hapuspo/{id}', [
	'uses' => 'POController@getDeletePO',
	'as' => 'hapuspo'
]);

Route::get('/ambilpo/{id}', [
	'uses' => 'POController@getEditPO',
	'as' => 'ambilpo'
]);

Route::post('/updatepo/{id}', [
	'uses' => 'POController@postEditPO',
	'as' => 'updatepo'
]);
//End route manajemen po

//Start route manajemen aktivitas
Route::post('/simpanaktivitas', [
	'uses' => 'AktivitasController@postAddAktivitas',
	'as' => 'simpanaktivitas'
]);

Route::get('/hapusaktivitas/{id}', [
	'uses' => 'AktivitasController@getDeleteAktivitas',
	'as' => 'hapusaktivitas'
]);

Route::get('/ambilaktivitas/{id}', [
	'uses' => 'AktivitasController@getEditAktivitas',
	'as' => 'ambilaktivitas'
]);

Route::post('/updateaktivitas/{id}', [
	'uses' => 'AktivitasController@postEditAktivitas',
	'as' => 'updateaktivitas'
]);

Route::get('/assigntk/{id}', [
	'uses' => 'AktivitasController@getAktivitas',
	'as' => 'assigntk'
]);

Route::post('/assigntk2/{id}/{id2}', [
	'uses' => 'AktivitasController@postAddTenagaKerja',
	'as' => 'assigntk2'
]);

Route::get('/hapustk/{id}/{id2}', [
	'uses' => 'AktivitasController@getDeleteTenagaKerja',
	'as' => 'hapustk'
]);
//End route manajemen aktivitas

//Start route pengeluaran
Route::get('/pilihproyek/{id}', [
	'uses' => 'PengeluaranController@getPilihProyek',
	'as' => 'pilihproyek'
]);

Route::get('/pilihpo/{id}/{id2}', [
	'uses' => 'PengeluaranController@getPilihPO',
	'as' => 'pilihpo'
]);

Route::get('/pilihaktivitas/{id}/{id2}/{id3}', [
	'uses' => 'PengeluaranController@getPilihAktivitas',
	'as' => 'pilihaktivitas'
]);

Route::post('/simpanpengeluaran', [
	'uses' => 'PengeluaranController@postAddPengeluaran',
	'as' => 'simpanpengeluaran'
]);

Route::get('/ambilpengeluaran/{id}', [
	'uses' => 'PengeluaranController@getEditPengeluaran',
	'as' => 'ambilpengeluaran'
]);

Route::post('/updatepengeluaran/{id}', [
	'uses' => 'PengeluaranController@postEditPengeluaran',
	'as' => 'updatepengeluaran'
]);

Route::get('/setujuipengeluaran/{id}', [
	'uses' => 'PengeluaranController@getSetujuiPengeluaran',
	'as' => 'setujuipengeluaran'
]);

Route::get('/batalpengeluaran/{id}', [
	'uses' => 'PengeluaranController@getBatalPengeluaran',
	'as' => 'batalpengeluaran'
]);
//End route pengeluaran
