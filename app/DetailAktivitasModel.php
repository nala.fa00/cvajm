<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailAktivitasModel extends Model
{
    protected $table = 'detailaktivitas';
    protected $fillable = [
        'id', 'id_aktivitas', 'id_karyawan',
    ];
    public $timestamps = false;
}
