<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaryawanModel extends Model
{
	protected $table = 'karyawan';
    protected $fillable = [
        'id', 'NIK', 'no_ktp', 'nama', 'gaji', 'tunjangan', 'jabatan','status',
    ];
    public $timestamps = false;
}
