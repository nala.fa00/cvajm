@extends('layouts.master')
@section('isi')
<div class="row">

        <div class="col-md-5 col-md-offset-1">
            <h1>Manajemen Karyawan</h1>
            @include('includes.pesan')
            <form action="{{ route('simpankaryawan') }}" method="post">
                <div class="form-group">
                	<label>NIK</label>
                	<input class="form-control" type="text" name="nik" id="nik" value="{{ $noselanjutnya }}" readonly>
                	<label>No. KTP</label>
                    <input class="form-control" type="text" name="noktp" id="noktp">
                    <label for="nama">Nama</label>
                    <input class="form-control" type="text" name="nama" id="nama">
                    <label for="password">Jabatan</label>
                	<select class="form-control" name="jabatan" id="jabatan">
						<option value="Eksekutif">Eksekutif</option>
						<option value="Administrasi">Administrasi</option>
						<option value="Pimpinan Proyek">Pimpinan Proyek</option>
						<option value="Teknisi">Teknisi</option>
						<option value="Driver">Driver</option>
					</select>
					<label for="gaji">Gaji Bulanan</label>
                    <input class="form-control" type="text" name="gaji" id="gaji">
                    <label for="tunjangan">Tunjangan</label>
                    <input class="form-control" type="text" name="tunjangan" id="tunjangan">
                    
                </div>
                <button type="submit" class="btn btn-primary">Register Karyawan</button>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="col-md-5">
        	<form class="form-inline" action="#" method="post">
				<div class="row">
				  	<div class="col-md-2"><label>Status</label></div>
				  	<div class="col-md-6">
				  		<select class="form-control" name="caristatus" id="caristatus" style="width: 100px;">
							<option value="OnSite">On Site</option>
							<option value="Idle">Idle</option>
						</select>
					</div>
				  	<div class="col-md-2 col-md-offset-1"><button type="submit" class="btn btn-primary">Cari</button></div>
				</div>
				
				
				{{ csrf_field() }}
        	</form>
        	
        	<table class="table table-striped">
					<thead>
						<th>NIK</th>
						<th>Nama Karyawan</th>
						<th>Jabatan</th>
						<th>Status</th>
					</thead>
					<tbody>
					@foreach($karyawan as $ambildata)
					   <tr>
                            <td>{{ $ambildata->NIK }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->jabatan }}</td>
                            <td>{{ $ambildata->status }}</td>
                            <td><a class="btn btn-primary" href="{{ route('ambilkaryawan',['id'=>$ambildata->id]) }}" role="button">Ubah</a></td>
                            <td><a class="btn btn-danger" href="{{ route('hapuskaryawan',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin hapus data?')" role="button">Hapus</a></td>
                       </tr>
                    @endforeach
					</tbody>

			</table>
			
        </div>      
    </div>
@endsection