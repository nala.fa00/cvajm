@extends('layouts.master')

@section('isi')
        <div class="col-md-5 col-md-offset-1">
            <h1>Ubah Karyawan</h1>
            @include('includes.pesan')
            <form action="{{ route('updatekaryawan', ['id'=>$datakaryawan->id]) }}" method="post">
                <div class="form-group">
                    <label for="nik">NIK : </label>
                    <label for="noktp">{{ $datakaryawan->NIK }}</label>
                    <br><label for="nama">Nama</label>
                    <input class="form-control" type="text" name="nama" id="nama" value='{{ $datakaryawan->nama }}'>
                    <label for="password">Jabatan</label>
                    <select class="form-control" name="jabatan" id="jabatan" value='{{ $datakaryawan->jabatan }}'>
                        <option value="Eksekutif">Eksekutif</option>
                        <option value="Administrasi">Administrasi</option>
                        <option value="Pimpinan Proyek">Pimpinan Proyek</option>
                        <option value="Teknisi">Teknisi</option>
                        <option value="Driver">Driver</option>
                    </select>
                    <label for="gaji">Gaji Bulanan</label>
                    <input class="form-control" type="text" name="gaji" id="gaji" value="{{ $datakaryawan->gaji }}">
                    <label for="tunjangan">Tunjangan</label>
                    <input class="form-control" type="text" name="tunjangan" id="tunjangan" value="{{ $datakaryawan->tunjangan }}">
                    
                </div>
                <button type="submit" class="btn btn-primary" onclick="return confirm('yakin ubah data?')">Ubah Karyawan</button>
                {{ csrf_field() }}
            </form>
        </div>
@endsection