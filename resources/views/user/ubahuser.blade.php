@extends('layouts.master')

@section('isi')
	<div class="row">
		<div class="col-md-6 col-md-offset-1">
			<h3>Ubah User</h3>
			<form action="{{ route('updateuser', ['id'=>$datauser->id]) }}" method="post">
				<div class="form-group">
					<label>Nama User : </label>
					<label for="namauser2">{{ $datauser->nama }}</label>
				</div>
				<div class="form-group">
					<label for="jabatan">Jabatan</label>
					<select class="form-control" name="jabatan" id="jabatan">
						<option value="Eksekutif">Eksekutif</option>
						<option value="Administrasi">Administrasi</option>
						<option value="Pimpinan Proyek">Pimpinan Proyek</option>
					</select>
				</div>
				<button type="submit" class="btn btn-primary"  onclick="return confirm('yakin ubah data?')">Ubah</button>
				{{ csrf_field() }}
			</form>
		</div>
	</div>
@endsection