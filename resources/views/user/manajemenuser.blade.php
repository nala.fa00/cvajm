@extends('layouts.master')

@section('isi')
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h1>Manajemen User</h1>
            @include('includes.pesan')
            <form action="{{route('simpanuser')}}" method="post">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input class="form-control" type="text" name="nama" id="nama">
                </div>
                <div class="form-group">
                    <label for="password">Hak Akses</label>
                	<select class="form-control" name="jabatan" id="jabatan">
						<option value="Eksekutif">Eksekutif</option>
						<option value="Administrasi">Administrasi</option>
						<option value="Kepala Proyek">Pimpinan Proyek</option>
					</select>
                </div>
                <button type="submit" class="btn btn-primary" onclick="return confirm('yakin simpan data?')" role="button" >Buat User</button>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="col-md-6">
        	<table class="table table-striped">
					<thead>
						<th>Nama User</th>
						<th>Jabatan</th>
					</thead>
					<tbody>
                        @foreach($user as $ambildata)
					   <tr>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->jabatan }}</td>
                            <td><a class="btn btn-primary" href="{{ route('ambiluser',['id'=>$ambildata->id]) }}" role="button">Ubah</a></td>
                            <td><a class="btn btn-danger" href="{{ route('hapususer',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin hapus data?')" role="button">Hapus</a></td>
                       </tr>
                       @endforeach
					</tbody>

				</table>
				
        </div>      
    </div>
<modal>
@endsection