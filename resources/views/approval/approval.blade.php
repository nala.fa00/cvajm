@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-8 col-md-offset-1">
            @include('includes.pesan')
            	<table class="table table-striped">
    				<thead>
    					<th>ID Pengeluaran</th>
                        <th>Nama Proyek</th>
    					<th>Nama PO</th>
                        <th>Nama Aktivitas</th>
                        <th>Nilai</th>
    					<th>Deskripsi</th>
    				</thead>
    				<tbody>
    				@foreach($pengeluaran as $ambildata)
    				   <tr>
                            <td>{{ $ambildata->id_pengeluaran }}</td>
                            <td>{{ $ambildata->namapro}}</td>
                            <td>{{ $ambildata->nama_po }}</td>
                            <td>{{ $ambildata->namaak }}</td>
                            <td>{{ $ambildata->nilai }}</td>
                            <td>{{ $ambildata->deskripsi }}</td>
                            <td><a class="btn btn-primary" href="{{ route('setujuipengeluaran',['id'=>$ambildata->id]) }}" role="button">Approve</a></td>
                            <td><a class="btn btn-danger" href="{{ route('batalpengeluaran',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin cancel?')" role="button">Decline</a></td>
                       </tr>
                    @endforeach
    				</tbody>

    			</table>
        </div>      
    </div>
@endsection