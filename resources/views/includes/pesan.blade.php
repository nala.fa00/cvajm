
@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
@endif

@if(Session::has('messagesalah'))
	<div class="alert alert-info">
		{{ Session::get('messagesalah') }}
    </div>
@endif

@if(Session::has('messagesukses'))
    <div class="alert alert-success">
        {{ Session::get('messagesukses') }}
    </div>
@endif

@if(Session::has('messagesukses2'))
    <div class="alert alert-success">
        {{ Session::get('messagesukses2') }}
    </div>
@endif
