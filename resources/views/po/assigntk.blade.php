@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-3 col-md-offset-1">
            <h1>Assign Tenaga Kerja</h1>
            
            <form action="#" method="post">
                <div class="form-group">
                	<label>ID Aktivitas</label>
                    <input class="form-control" type="text" name="idaktivitas" id="idaktivitas" value="{{ $aktivitas->id_aktivitas }}" readonly>
                    <label>Nama Aktivitas</label>
                    <input class="form-control" type="text" name="namaaktivitas" id="namaaktivitas" value="{{ $aktivitas->nama }}" readonly>
                    <label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai" value="{{ $aktivitas->mulai }}" readonly="">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai" value="{{ $aktivitas->selesai }}" readonly>
                    <label>Kegiatan</label>
                    <input class="form-control" type="text" name="kegiatan" id="kegiatan" value="{{ $aktivitas->kegiatan }}" readonly>
                </div>
                
            </form>
        </div>
        <div class="col-md-8">
        	<table class="table table-striped">
					<thead>
						<th>NIK</th>
						<th>Nama</th>
                        <th>Posisi</th>
                        <th>Status</th>
						
					</thead>
					<tbody>
					@foreach($karyawan as $ambildata)
                       <tr>
                        <form action="{{ route('assigntk2',['id'=>$ambildata->id, 'id2'=>$aktivitas->id]) }}" method="post">
                            <td>{{ $ambildata->NIK }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->jabatan }}</td>
                            <td>{{ $ambildata->status }}</td>
                            <td><button type="submit" class="btn btn-primary">Assign</button></td>
                            {{ csrf_field() }}
                        </form>
                       </tr>
                    @endforeach
					</tbody>

			</table>
        </div>      
</div>
<div class="row">
        <div class="col-md-3 col-md-offset-1">
            <h3>TK Di-Assign</h3>
            <table class="table table-striped">
                    <thead>
                        <th>NIK</th>
                        <th>Nama</th>
                        
                    </thead>
                    <tbody>
                    @foreach($assign as $ambildata)
                       <tr>
                            <td>{{ $ambildata->NIK }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            
                            <td><a class="btn btn-default" href="{{ route('hapustk',['id'=>$ambildata->id, 'id2'=>$ambildata->idkar]) }}" role="button">Cancel</a></td>
                       </tr>
                    @endforeach
                    </tbody>

            </table>
                    
        </div>
</div>
@endsection