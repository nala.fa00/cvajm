@extends('layouts.master')

@section('isi')
        <div class="col-md-5 col-md-offset-1">
            <h1>Ubah Purchase Order</h1>
            
            <form action="{{ route('updatepo', ['id'=>$datapo->id]) }}" method="post">
                <div class="form-group">
                    <label>ID PO</label>
                    <input class="form-control" type="text" name="idpo" id="idpo" value="{{ $datapo->id_po }}" readonly>
                    <label>Nama PO</label>
                    <input class="form-control" type="text" name="namapo" id="namapo" value="{{ $datapo->nama_po }}">
                    <label>Nama Proyek</label>
                    <select class="form-control" name="proyek" id="proyek" value="{{ $datapo->namapro }}">
                        @foreach($dataproyek as $ambildata)
                            <option value="{{ $ambildata->id }}">{{ $ambildata->nama }}</option>
                        @endforeach
                    </select>
                    <label>Nama Perusahaan</label>
                    <input class="form-control" type="text" name="namaperusahaan" id="namaperusahaan" value="{{ $datapo->nama_perusahaan }}">
                    <label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai" value="{{ $datapo->mulai }}">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai" value="{{ $datapo->selesai }}">
                    <label>Biaya</label>
                    <input class="form-control" type="text" name="biaya" id="biaya" value="{{ $datapo->biaya }}">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status" value="{{ $datapo->status }}">
                        <option value="Berjalan">Berjalan</option>
                        <option value="Selesai">Selesai</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update PO</button>
                {{ csrf_field() }}
            </form>
        </div>
@endsection