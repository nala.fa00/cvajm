@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-3 col-md-offset-1">
            <h1>Manajemen Aktivitas</h1>
            @include('includes.pesan')
            <form action="{{ route('simpanaktivitas') }}" method="post">
                <div class="form-group">
                	<label>ID Aktivitas</label>
                    <input class="form-control" type="text" name="idaktivitas" id="idaktivitas" value="{{ $noselanjutnya }}" readonly>
                    <label>Nama Aktivitas</label>
                    <input class="form-control" type="text" name="namaaktivitas" id="namaaktivitas">
                    <label>Nama PO</label>
                    <select class="form-control" name="po" id="po">
                        @foreach($datapo as $ambildata)
                            <option value="{{ $ambildata->id }}">{{ $ambildata->nama_po }}</option>
                        @endforeach
                    </select>
					<label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai">
                    <label>Kegiatan</label>
                    <input class="form-control" type="text" name="kegiatan" id="kegiatan">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status">
                        <option value="Berjalan">Berjalan</option>
                        <option value="Selesai">Selesai</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Register Activity</button>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="col-md-8">
        	<table class="table table-striped">
					<thead>
						<th>ID Aktivitas</th>
						<th>Nama Aktivitas</th>
                        <th>Nama Proyek</th>
                        <th>Nama PO</th>
						<th>Tgl. Mulai</th>
						<th>Tgl. Selesai</th>
                        <th>Kegiatan</th>
						<th>Status</th>
					</thead>
					<tbody>
					@foreach($aktivitas as $ambildata)
                       <tr>
                            <td>{{ $ambildata->id_aktivitas }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->namapro }}</td>
                            <td>{{ $ambildata->namapo }}</td>
                            <td>{{ $ambildata->mulai }}</td>
                            <td>{{ $ambildata->selesai }}</td>
                            <td>{{ $ambildata->kegiatan }}</td>
                            <td>{{ $ambildata->status }}</td>
                            @if($ambildata->status == 'Berjalan')
                            <td><a class="btn btn-default" href="{{ route('assigntk',['id'=>$ambildata->id]) }}" role="button">Assign TK</a></td>
                            <td><a class="btn btn-primary" href="{{ route('ambilaktivitas',['id'=>$ambildata->id]) }}" role="button">Ubah</a></td>
                            <td><a class="btn btn-danger" href="{{ route('hapusaktivitas',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin hapus data?')" role="button">Hapus</a></td>
                            @endif
                       </tr>
                    @endforeach
					</tbody>

			</table>
			
        </div>      
    </div>
@endsection