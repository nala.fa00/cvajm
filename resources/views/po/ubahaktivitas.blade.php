@extends('layouts.master')

@section('isi')
        <div class="col-md-5 col-md-offset-1">
            <h1>Ubah Purchase Order</h1>
            
            <form action="{{ route('updateaktivitas', ['id'=>$dataaktivitas->id]) }}" method="post">
                <div class="form-group">
                    <label>ID Aktivitas</label>
                    <input class="form-control" type="text" name="idaktivitas" id="idaktivitas" value="{{ $dataaktivitas->id_aktivitas }}" readonly>
                    <label>Nama Aktivitas</label>
                    <input class="form-control" type="text" name="namaaktivitas" id="namaaktivitas" value="{{ $dataaktivitas->nama }}">
                    <label>Nama PO</label>
                    <select class="form-control" name="po" id="po" value="{{ $dataaktivitas->id_po }}">
                        @foreach($datapo as $ambildata)
                            <option value="{{ $ambildata->id }}">{{ $ambildata->nama_po }}</option>
                        @endforeach
                    </select>
                    <label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai" value="{{ $dataaktivitas->mulai }}">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai" value="{{ $dataaktivitas->selesai }}">
                    <label>Kegiatan</label>
                    <input class="form-control" type="text" name="kegiatan" id="kegiatan" value="{{ $dataaktivitas->kegiatan }}">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status" value="{{ $dataaktivitas->status }}">
                        <option value="Berjalan">Berjalan</option>
                        <option value="Selesai">Selesai</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update Aktivitas</button>
                {{ csrf_field() }}
            </form>
        </div>
@endsection