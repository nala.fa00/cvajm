@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h1>Manajemen Purchase Order</h1>
            
            <form action="{{ route('simpanpo') }}" method="post">
                <div class="form-group">
                	<label>ID PO</label>
                    <input class="form-control" type="text" name="idpo" id="idpo" value="{{ $noselanjutnya }}" readonly>
                    <label>Nama PO</label>
                    <input class="form-control" type="text" name="namapo" id="namapo">
                    <label>Nama Proyek</label>
                    <select class="form-control" name="proyek" id="proyek">
						@foreach($dataproyek as $ambildata)
							<option value="{{ $ambildata->id }}">{{ $ambildata->nama }}</option>
						@endforeach
					</select>
					<label>Nama Perusahaan</label>
                    <input class="form-control" type="text" name="namaperusahaan" id="namaperusahaan">
					<label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai">
                    <label>Biaya</label>
                    <input class="form-control" type="text" name="biaya" id="biaya">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status">
						<option value="Berjalan">Berjalan</option>
						<option value="Selesai">Selesai</option>
					</select>
                </div>
                <button type="submit" class="btn btn-primary">Register PO</button>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="col-md-5">
        	<form class="form-inline" action="#" method="post">
				<div class="row">
				  	<div class="col-md-2"><label>Status</label></div>
				  	<div class="col-md-6">
				  		<select class="form-control" name="caristatus" id="caristatus" style="width: 100px;">
							<option value="OnSite">Berjalan</option>
							<option value="Idle">Selesai</option>
						</select>
					</div>
				  	<div class="col-md-2 col-md-offset-1"><button type="submit" class="btn btn-primary">Cari</button></div>
				</div>
				
				
				{{ csrf_field() }}
        	</form>
        	
        	<table class="table table-striped">
					<thead>
						<th>ID PO</th>
						<th>Nama PO</th>
						<th>Nama Proyek</th>
						<th>Tgl. Mulai</th>
						<th>Tgl. Selesai</th>
						<th>Status</th>
					</thead>
					<tbody>
					@foreach($po as $ambildata)
					   <tr>
                            <td>{{ $ambildata->id_po }}</td>
                            <td>{{ $ambildata->nama_po }}</td>
                            <td>{{ $ambildata->namapro }}</td>
                            <td>{{ $ambildata->mulai }}</td>
                            <td>{{ $ambildata->selesai }}</td>
                            <td>{{ $ambildata->status }}</td>
                            @if($ambildata->status == 'Berjalan')
                            <td><a class="btn btn-primary" href="{{ route('ambilpo',['id'=>$ambildata->id]) }}" role="button">Ubah</a></td>
                            <td><a class="btn btn-danger" href="{{ route('hapuspo',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin hapus data?')" role="button">Hapus</a></td>
                            @endif
                       </tr>
                    @endforeach
					</tbody>

			</table>
			
        </div>      
    </div>
@endsection