@extends('layouts.master')

@section('isi')
        <div class="col-md-5 col-md-offset-1">
            <h1>Ubah Proyek</h1>
            
            <form action="{{ route('updateproyek', ['id'=>$dataproyek->id]) }}" method="post">
                <div class="form-group">
                    <label>ID Proyek</label>
                    <input class="form-control" type="text" name="idproyek" id="idproyek" value="{{ $dataproyek->id_proyek }}" readonly>
                    <label>Nama Proyek</label>
                    <input class="form-control" type="text" name="namaproyek" id="namaproyek" value="{{ $dataproyek->nama }}">
                    <label>Koordinator Proyek</label>
                    <select class="form-control" name="koordinatorproyek" id="koordinatorproyek" value="{{ $dataproyek->namakoor }}">
                        @foreach($datakoordinator as $ambildata)
                            <option value="{{ $ambildata->id }}">{{ $ambildata->nama }}</option>
                        @endforeach
                    </select>
                    <label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai" value="{{ $dataproyek->mulai }}">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai" value="{{ $dataproyek->selesai }}">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status" value="{{ $dataproyek->status }}">
                        <option value="Berjalan">Berjalan</option>
                        <option value="Selesai">Selesai</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Update Proyek</button>
                {{ csrf_field() }}
            </form>
        </div>
@endsection