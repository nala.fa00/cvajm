@extends('layouts.master')
@section('isi')
<form action="{{ route('updatepengeluaran', ['id'=>$pengeluaran->id]) }}" method="post">
<div class="row">
    <div class="col-md-5 col-md-offset-1">
            <h2>Edit Pengeluaran</h2>
            <label>ID Pengeluaran</label>
            <input class="form-control" type="text" name="idpengeluaran" id="idpengeluaran" value="{{ $pengeluaran->id_pengeluaran }}" readonly>
            <label>Keperluan</label>
            <input class="form-control" type="text" name="keperluan" id="keperluan" value="{{ $pengeluaran->deskripsi }}">
            <label>Nominal</label>
            <input class="form-control" type="text" name="nominal" id="nominal" value="{{ $pengeluaran->nilai }}">
            <br>
            <button type="submit" class="btn btn-primary">Ubah Pengeluaran</button>
            {{ csrf_field() }}
        
    </div>
</div>
</form>
@endsection