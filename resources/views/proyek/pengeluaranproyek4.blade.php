@extends('layouts.master')
@section('isi')
<form action="{{ route('simpanpengeluaran') }}" method="post">
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <h2>Data Proyek</h2>
            <label>ID Proyek</label>
            <input class="form-control" type="text" name="idproyek" id="idproyek" value="{{ $proyek->id_proyek }}" readonly>
            <label>Nama Proyek</label>
            <input class="form-control" type="text" name="namaproyek" id="namaproyek" value="{{ $proyek->nama }}" readonly>
    </div>
    <div class="col-md-3">
        <h2>Data PO</h2>
            <label>ID PO</label>
            <input class="form-control" type="text" name="idpo" id="idpo" value="{{ $po->id_po }}" readonly>
            <label>Nama PO</label>
            <input class="form-control" type="text" name="namapo" id="namapo" value="{{ $po->nama_po }}" readonly>
    </div>
    <div class="col-md-3">
        <h2>Data Aktivitas</h2>
            <label>ID Aktivitas</label>
            <input class="form-control" type="text" name="idaktivitas" id="idaktivitas" value="{{ $aktivitas->id_aktivitas }}" readonly>
            <input class="form-control" type="text" name="idak" id="idak" value="{{ $aktivitas->id }}">
            <label>Nama Aktivitas</label>
            <input class="form-control" type="text" name="namaaktivitas" id="namaaktivitas" value="{{ $aktivitas->nama }}" readonly>
    </div>
    
</div>
<div class="row">
    <div class="col-md-5 col-md-offset-1">
            <h2>Input Pengeluaran</h2>
            <label>ID Pengeluaran</label>
            <input class="form-control" type="text" name="idpengeluaran" id="idpengeluaran" value="{{ $noselanjutnya }}" readonly>
            <label>Keperluan</label>
            <input class="form-control" type="text" name="keperluan" id="keperluan">
            <label>Nominal</label>
            <input class="form-control" type="text" name="nominal" id="nominal">
            <br>
            <button type="submit" class="btn btn-primary">Ajukan Pengeluaran</button>
            {{ csrf_field() }}
        
    </div>
</div>
</form>
@endsection