@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-5 col-md-offset-1">
            <h1>Manajemen Proyek</h1>
            @include('includes.pesan')
            <!-- Sebelah Kiri -->
            <form action="{{ route('simpanproyek') }}" method="post">
                <div class="form-group">
                	<label>ID Proyek</label>
                    <input class="form-control" type="text" name="idproyek" id="idproyek" value="{{ $noselanjutnya }}" readonly>
                    <label>Nama Proyek</label>
                    <input class="form-control" type="text" name="namaproyek" id="namaproyek">
                    <label>Koordinator Proyek</label>
                  	<select class="form-control" name="koordinatorproyek" id="koordinatorproyek">
						@foreach($datakoordinator as $ambildata)
							<option value="{{ $ambildata->id }}">{{ $ambildata->nama }}</option>
						@endforeach
					</select>
					<label>Tanggal Mulai</label>
                    <input class="form-control" type="date" name="tanggalmulai" id="tanggalmulai">
                    <label>Tanggal Selesai</label>
                    <input class="form-control" type="date" name="tanggalselesai" id="tanggalselesai">
                    <label>Status</label>
                    <select class="form-control" name="status" id="status">
						<option value="Berjalan">Berjalan</option>
						<option value="Selesai">Selesai</option>
					</select>
                </div>
                <button type="submit" class="btn btn-primary">Register Proyek</button>
                {{ csrf_field() }}
            </form>
        </div>
        <div class="col-md-6">
        	<form class="form-inline" action="#" method="post">
				<div class="row">
				  	<div class="col-md-2"><label>Status</label></div>
				  	<div class="col-md-6">
				  		<select class="form-control" name="caristatus" id="caristatus" style="width: 100px;">
							<option value="OnSite">Berjalan</option>
							<option value="Idle">Selesai</option>
                            <option value="Cancel">Cancel</option>
						</select>
					</div>
				  	<div class="col-md-2 col-md-offset-1"><button type="submit" class="btn btn-primary">Cari</button></div>
				</div>
				
				
				{{ csrf_field() }}
        	</form>
        	
        	<table class="table table-striped">
					<thead>
						<th>ID Proyek</th>
						<th>Nama Proyek</th>
						<th>Tgl. Mulai</th>
						<th>Tgl. Selesai</th>
						<th>Pimpinan Proyek</th>
						<th>Status</th>
					</thead>
					<tbody>
					@foreach($proyek as $ambildata)
					   <tr>
                            <td>{{ $ambildata->id_proyek }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->mulai }}</td>
                            <td>{{ $ambildata->selesai }}</td>
                            <td>{{ $ambildata->namakoor }}</td>
                            <td>{{ $ambildata->status }}</td>
                            @if($ambildata->status == 'Berjalan')
                            <td><a class="btn btn-primary" href="{{ route('ambilproyek',['id'=>$ambildata->id]) }}" role="button">Ubah</a></td>
                            <td><a class="btn btn-danger" href="{{ route('hapusproyek',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin cancel proyek?')" role="button">Cancel</a></td>
                            @endif
                            <td><a class="btn btn-default" href="{{ route('detailproyek',['id'=>$ambildata->id]) }}" role="button">Detail</a></td>
                       </tr>
                    @endforeach
					</tbody>

			</table>
			
        </div>      
    </div>
@endsection