@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-8 col-md-offset-1">
            @include('includes.pesan')
            	<table class="table table-striped">
    				<thead>
    					<th>ID Pengeluaran</th>
                        <th>Nama Proyek</th>
    					<th>Nama PO</th>
                        <th>Nama Aktivitas</th>
                        <th>Nilai</th>
    					<th>Deskripsi</th>
                        <th>Status</th>
    				</thead>
    				<tbody>
    				@foreach($pengeluaran as $ambildata)
    				   <tr>
                            <td>{{ $ambildata->id_pengeluaran }}</td>
                            <td>{{ $ambildata->namapro}}</td>
                            <td>{{ $ambildata->nama_po }}</td>
                            <td>{{ $ambildata->namaak }}</td>
                            <td>{{ $ambildata->nilai }}</td>
                            <td>{{ $ambildata->deskripsi }}</td>
                            <td>{{ $ambildata->status }}</td>
                            <td><a class="btn btn-primary" href="{{ route('ambilpengeluaran',['id'=>$ambildata->id]) }}" role="button">Edit</a></td>
                            </td>
                       </tr>
                    @endforeach
    				</tbody>

    			</table>
        </div>      
    </div>
@endsection