@extends('layouts.master')
@section('isi')
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <h2>Data Proyek</h2>
            <label>ID Proyek</label>
            <input class="form-control" type="text" name="idproyek" id="idproyek" value="{{ $proyek->id_proyek }}" readonly>
            <label>Nama Proyek</label>
            <input class="form-control" type="text" name="namaproyek" id="namaproyek" value="{{ $proyek->nama }}" readonly>
    </div>
    <div class="col-md-3">
        <h2>Data PO</h2>
            <label>ID PO</label>
            <input class="form-control" type="text" name="idpo" id="idpo" value="{{ $po->id_po }}" readonly>
            <label>Nama PO</label>
            <input class="form-control" type="text" name="namapo" id="namapo" value="{{ $po->nama_po }}" readonly>
    </div>
    <div class="col-md-3">
        <h2>Pilih Aktivitas</h2>
        <table class="table table-striped">
            <thead>
                <th>ID Aktivitas</th>
                <th>Nama Aktivitas</th>
            </thead>
            <tbody>
                    @foreach($aktivitas as $ambildata)
                       <tr>
                            <td>{{ $ambildata->id_aktivitas }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td><a class="btn btn-primary" href="{{ route('pilihaktivitas',['id'=>$proyek->id, 'id2'=>$po->id, 'id3'=>$ambildata->id]) }}" role="button">Pilih</a></td>
                       </tr>
                    @endforeach
            </tbody>

        </table>
    </div>
</div>
@endsection