@extends('layouts.master')
@section('isi')
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <h2>Data Proyek</h2>
            <label>ID Proyek</label>
            <input class="form-control" type="text" name="idproyek" id="idproyek" value="{{ $proyek->id_proyek }}" readonly>
            <label>Nama Proyek</label>
            <input class="form-control" type="text" name="namaproyek" id="namaproyek" value="{{ $proyek->nama }}" readonly>
    </div>
    <div class="col-md-3">
        <h2>Pilih PO</h2>
        <table class="table table-striped">
            <thead>
                <th>ID PO</th>
                <th>Nama PO</th>
            </thead>
            <tbody>
                    @foreach($po as $ambildata)
                       <tr>
                            <td>{{ $ambildata->id_po }}</td>
                            <td>{{ $ambildata->nama_po }}</td>
                            <td><a class="btn btn-primary" href="{{ route('pilihpo',['id'=>$proyek->id, 'id2'=>$ambildata->id]) }}" role="button">Pilih</a></td>
                       </tr>
                    @endforeach
            </tbody>

        </table>
    </div>
</div>
@endsection