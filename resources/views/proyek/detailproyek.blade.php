@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-8 col-md-offset-1">
            <h1>Detail Proyek</h1>
            @include('includes.pesan')
        	<table class="table table-striped">
					<thead>
						<th>ID Proyek</th>
						<th>Nama Proyek</th>
                        <th>Tgl. Mulai</th>
                        <th>Tgl. Selesai</th>
						<th>Pimpinan Proyek</th>
                        <th>Status</th>
					</thead>
					<tbody>
                       <tr>
                            <td>{{ $dataproyek->id_proyek }}</td>
                            <td>{{ $dataproyek->nama }}</td>
                            <td>{{ $dataproyek->mulai }}</td>
                            <td>{{ $dataproyek->selesai }}</td>
                            <td>{{ $dataproyek->namakoor }}</td>
                            <td>{{ $dataproyek->status }}</td>
                       </tr>
					</tbody>
			</table>

            <table class="table table-striped">
                    <thead>
                        <th>ID PO</th>
                        <th>Nama PO</th>
                        <th>Tgl. Mulai</th>
                        <th>Tgl. Selesai</th>
                        <th>Status</th>
                    </thead>
                    <tbody>
                        @foreach($datapo as $ambildata)
                        <tr>
                            <td>{{ $ambildata->id_po }}</td>
                            <td>{{ $ambildata->nama_po }}</td>
                            <td>{{ $ambildata->mulai }}</td>
                            <td>{{ $ambildata->selesai }}</td>
                            <td>{{ $ambildata->status }}</td>
                        </tr>
                        @endforeach
                    </tbody>
            </table>
			
            <table class="table table-striped">
                <thead>
                    <th>ID Aktivitas</th>
                    <th>Nama</th>
                    <th>Mulai</th>
                    <th>Selesai</th>
                    <th>Kegiatan</th>
                    <th>Status</th>
                </thead>
                <tbody>
                        @foreach($dataaktivitas as $ambildata)
                        <tr>
                            <td>{{ $ambildata->id_aktivitas }}</td>
                            <td>{{ $ambildata->aknama }}</td>
                            <td>{{ $ambildata->akmulai }}</td>
                            <td>{{ $ambildata->akselesai }}</td>
                            <td>{{ $ambildata->akkegiatan }}</td>
                            <td>{{ $ambildata->akstatus }}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>

            <table class="table table-striped">
                <thead>
                    <th>ID Pengeluaran</th>
                    <th>Nilai</th>
                    <th>Deskripsi</th>
                    <th>Status</th>
                </thead>
                <tbody>
                        @foreach($datapengeluaran as $ambildata)
                        <tr>
                            <td>{{ $ambildata->id_pengeluaran }}</td>
                            <td>{{ $ambildata->nilai }}</td>
                            <td>{{ $ambildata->deskripsi }}</td>
                            <td>{{ $ambildata->statuspeng }}</td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>      
    </div>
@endsection