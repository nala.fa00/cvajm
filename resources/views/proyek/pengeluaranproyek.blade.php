@extends('layouts.master')
@section('isi')
<div class="row">
    <div class="col-md-3 col-md-offset-1">
        <h2>Pilih Proyek</h2>
        @include('includes.pesan')
        <table class="table table-striped">
                    <thead>
                        <th>ID Proyek</th>
                        <th>Nama Proyek</th>
                    </thead>
                    <tbody>
                    @foreach($proyek as $ambildata)
                       <tr>
                            <td>{{ $ambildata->id_proyek }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td><a class="btn btn-primary" href="{{ route('pilihproyek',['id'=>$ambildata->id]) }}" role="button">Pilih</a></td>
                       </tr>
                    @endforeach
                    </tbody>

            </table>
    </div>
</div>
@endsection