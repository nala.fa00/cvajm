@extends('layouts.master')
@section('isi')
<div class="row">
        <div class="col-md-10 col-md-offset-1">
            @include('includes.pesan')
            	<table class="table table-striped">
    				<thead>
    					<th>ID Proyek</th>
    					<th>Nama Proyek</th>
                        <th>Koordinator</th>
                        <th>Tanggal Mulai</th>
    					<th>Tanggal Selesai</th>
                        <th>Status</th>
    				</thead>
    				<tbody>
    				@foreach($proyek as $ambildata)
    				   <tr>
                            <td>{{ $ambildata->id_proyek }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->nama }}</td>
                            <td>{{ $ambildata->mulai }}</td>
                            <td>{{ $ambildata->selesai }}</td>
                            <td>{{ $ambildata->status }}</td>
                            <td><a class="btn btn-primary" href="{{ route('setujuipengeluaran',['id'=>$ambildata->id]) }}" role="button">Approve</a></td>
                            <td><a class="btn btn-danger" href="{{ route('batalpengeluaran',['id'=>$ambildata->id]) }}" onclick="return confirm('yakin cancel?')" role="button">Decline</a></td>
                       </tr>
                    @endforeach
    				</tbody>

    			</table>
        </div>      
    </div>
@endsection