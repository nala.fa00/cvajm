<header>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <img src="{{asset('logo.png')}}" style="padding-top: 10px;">
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">User</a>
          <ul class="dropdown-menu">
            <li><a href="{{route('manajemenuser')}}">Manajemen User</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Karyawan</a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('manajemenkaryawan') }}">Manajemen Karyawan</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Proyek</a>
          <ul class="dropdown-menu">
            <li><a href="{{route('manajemenproyek')}}">Manajemen Proyek</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Pengeluaran Proyek</a>
          <ul class="dropdown-menu">
            <li><a href="{{route('pengeluaranproyek')}}">Pengeluaran Proyek</a></li>
            <li><a href="{{ route('approvalpengeluaran') }}">Approval Pengeluaran</a></li>
            <li><a href="{{ route('listpengeluaran') }}">List Pengeluaran</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Purchase Order</a>
          <ul class="dropdown-menu">
            <li><a href="{{route('manajemenpo')}}">Manajemen Purchase Order</a></li>
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Aktivitas</a>
          <ul class="dropdown-menu">
            <li><a href="{{route('manajemenaktivitas')}}">Manajemen Aktivitas</a></li>
          </ul>
        </li>

      </ul>

      <!--navbar sebelah kanan-->
      <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Nama user <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Ubah Password</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

</header>